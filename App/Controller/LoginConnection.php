<?php

namespace App;
use Library;
use Library\View;
use PDO;
use \Session;

class LoginConnection {

	public function __construct() {
		$obj = new Library\Guard();
		$obj->isAuthtenticated();
	}


	public function login() {

		$username = isset($_POST["username"]) ? $_POST["username"] : "";
		$password = isset($_POST["password"]) ? $_POST["password"] : "";


		if($username == "" || $password == "") {
			echo $this->response(false,"Credentials are invalid.");
			return;
		} 

		$hashed_password = password_hash($password, PASSWORD_DEFAULT);
		try {
			$db = new ConnectionClass();
			$query = $db->DBConnect()->prepare('SELECT * FROM users WHERE username = ?');
			$query->execute([$username]);

			if($queryResult = $query->fetch(PDO::FETCH_OBJ)) {	
				if(password_verify($password, $queryResult->password)) {
				    $_SESSION["login"] = $queryResult;
					View::redirect("/home");
					return;
				} else{
					return View::redirect("/login");
				}
				
			}
			else {
				return View::redirect("/login");
			}
		}
		catch(PDOException $e) {
			echo $e->getMessage();
		}		
	}
	public function logout() {
		unset($_SESSION['login']);
		return View::redirect("/login");
	}

	public function loginPage() {

		return View::response("pages/login/login.php");
	}


	public function response($status, $message) {
		$result = array();
		$result["status"] = $status;
		$result["message"] = $message;
		return json_encode($result);
	}

}
