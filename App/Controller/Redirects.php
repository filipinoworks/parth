<?php
namespace App;
use Library;
use Library\View;

class Redirects {

	public function notFound() {
		return View::response("pages/404.php");
	}
	public function conflict_request() {
		return View::response("pages/conflict.php");
	}
}
