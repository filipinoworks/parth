<?php
namespace App;
use Library;
use Library\View;

class Homepage {

	public function __construct() {
		$obj = new Library\Guard();
		$obj->isAuthtenticated();
		
	}

	public function home() {
		return View::response("pages/index.php");
	}
}
