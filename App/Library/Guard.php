<?php
namespace Library;
use \Sessions;


class Guard {
	public function isAuthtenticated() {
		if(Router::$current_url == "login" && Router::$request_type == "GET") {
			if(isset($_SESSION["login"])) {
				return View::redirect("/");
				die;
			}
			View::response("pages/login/login.php");
			die;
		}
		if(!isset($_SESSION["login"])) {
			return View::redirect("/login");
			die;
		}
	}
}