<?php
namespace Library;

class Router {
	static $routes = array();
	static $current_url = "";
	static $request_type = "GET";

	public static function get($url, $controlerMethod){
		$request_type = "GET";

		$data = array();
		$data["url"] = $url;
		$data["request_type"] = $request_type;
		$data["controllerMethod"] = $controlerMethod;
		$unregistered_route = $data;
		array_push(self::$routes, $unregistered_route);
	}

	public static function post($url, $controlerMethod){

		$request_type = "POST";

		$data = array();
		$data["url"] = $url;
		$data["request_type"] = $request_type;
		$data["controllerMethod"] = $controlerMethod;
		$unregistered_route = $data;
		array_push(self::$routes, $unregistered_route);
	}
}

