<?php 
namespace Library;

Router::get("/home","Homepage@home");
Router::get("/","Homepage@home");
Router::get("/404","Redirects@notFound");
Router::get("/login","LoginConnection@loginPage");
Router::post("/login","LoginConnection@login");

Router::get("/logout","LoginConnection@logout");
Router::get("/conflict_request","Redirects@conflict_request");

