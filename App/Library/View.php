<?php
namespace Library;
use Library\View;

class View {
	
	public static function response($view_url) {
		require "../Views/".$view_url;
	} 

	public static function show($view_url) {
		require "../Views/".$view_url;
	} 

	public static function redirect($url) {
		if(substr($url, 0,1) == "/") {
			if(strlen($url) > 1) {
				$url = substr($url, 1,100);
			}
		}
		header("Location: $url");
	}
}