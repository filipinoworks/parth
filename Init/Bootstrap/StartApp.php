<?php
namespace Init;
use App;
use Library\View;
use Library\Router;


class StartApp extends Router{
	
	public function init() {
		session_start();
		$this->processUrl();
	}
	
	public function processUrl() {

		if(!isset($_GET["path"])) {
			$url = "/";
		}else{
			$url = $_GET["path"];
		}


		foreach (self::$routes as $route_key => $route) {

			if(substr($route["url"], 0,1) == "/") {
				if(strlen($route["url"]) > 1) {
					$route["url"] = substr($route["url"], 1,100);
				}
			}

			$same_url_count = 0;

			self::$current_url = $route["url"];
			self::$request_type = $_SERVER['REQUEST_METHOD'];

			if($route["url"] == $url){

				if ($_SERVER['REQUEST_METHOD'] != $route["request_type"]) {
				    $same_url_count++;
				    continue;
				}

				$controllerMethod = array_filter(explode('@', $route["controllerMethod"]));
				$controller = $controllerMethod[0];
				$method = $controllerMethod[1];
				$class = "App\\$controller";
				$obj = new $class();
				return $obj->$method();
			}
		}

		if($same_url_count > 1) {
			View::response("/conflict_request");
			die;
		}

		View::redirect("/404");
		die;
	}
}

